# ci-distcheck

Creates tarballs and performs checks.

# The build environments

This project relies on the build environments created by the 'ci-buildtools'
project.

# The created tarball

The newest created (snapshot) tarball is available through the Gitlab UI
or directly for download: [CI/CD > Jobs > Finished (check-cached) > Job artifacts > Browse > libffcall-snapshot.tar](https://gitlab.com/gnu-libffcall/ci-distcheck/-/jobs/artifacts/master/raw/libffcall-snapshot.tar?job=check-cached).
